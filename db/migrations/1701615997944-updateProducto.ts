import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateProducto1701615997944 implements MigrationInterface {
    name = 'UpdateProducto1701615997944'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "products" DROP COLUMN "images"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "products" ADD "images" text NOT NULL`);
    }

}
