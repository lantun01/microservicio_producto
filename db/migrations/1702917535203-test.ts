import { MigrationInterface, QueryRunner } from "typeorm";

export class Test1702917535203 implements MigrationInterface {
    name = 'Test1702917535203'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "products" DROP CONSTRAINT "FK_745aa540782dd7e77b386f390d7"`);
        await queryRunner.query(`ALTER TABLE "products" RENAME COLUMN "nameUserId" TO "testID"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "products" RENAME COLUMN "testID" TO "nameUserId"`);
        await queryRunner.query(`ALTER TABLE "products" ADD CONSTRAINT "FK_745aa540782dd7e77b386f390d7" FOREIGN KEY ("nameUserId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
