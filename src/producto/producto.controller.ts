import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProductoService } from './producto.service';
import { CreateProductoDto } from './dto/create-producto.dto';
import { UpdateProductoDto } from './dto/update-producto.dto';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';

@Controller('producto')
export class ProductoController {
  constructor(private readonly productoService: ProductoService) {}

  @MessagePattern('Create_Product')
  Create(@Payload() create: CreateProductoDto) {
    return this.productoService.create(create);
  }

  @MessagePattern('findAllProduct')
  find() {
    return this.productoService.findAll();
  }

  @MessagePattern('findOneProducto')
  findOne(@Payload() id: number) {
    return this.productoService.findOne(id);
  }

  @MessagePattern('ProductoUpdate')
  update(@Payload() update:  UpdateProductoDto) {
    return this.productoService.update(update);
  }
  
  ////////////////////////////////////////////////////////////////////
  @MessagePattern({cmd: 'greeting'})
  getGreetingMessage(name: string): string {
    return `Hello tunometecabgra ${name}`;
  }

  @MessagePattern({cmd: 'greeting-async'})
  async getGreetingMessageAysnc(name: string): Promise<string> {
    return `Hello ${name} Async`;
  }

  @EventPattern('book-created')
  async handleBookCreatedEvent(data: Record<string, unknown>) {
    console.log(data);
  }
}
