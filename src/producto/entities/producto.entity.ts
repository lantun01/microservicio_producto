import { UserEntity } from "src/entities/user.entity";
import { Column, CreateDateColumn, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn, Timestamp, UpdateDateColumn } from "typeorm";

@Entity({name:'products'})
export class Producto {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    title:string;

    @Column()
    description:string;

    @Column({type:'decimal'})
    price:number;

    @Column()
    stock:number;

    //@Column('simple-array')
    //images: string[];

    @Column()
    category:string;

    @CreateDateColumn()
    createAt:Timestamp;

    @UpdateDateColumn()
    updateAt:Timestamp;

    @Column({nullable: true})
    testID: number;

}
