import { IsArray, IsNotEmpty, IsNumber, IsPositive, IsString, Min } from "class-validator";

export class CreateProductoDto {
    @IsNotEmpty({message:'el titulo no puede estar vacio'})
    @IsString()
    title:string;

    @IsNotEmpty({message:'la descripcion no puede estar vacia'})
    @IsString()
    description:string;

    @IsNotEmpty({message:'el precio no puede ser cero'})
    @IsNumber({maxDecimalPlaces:2},{message:'el precio debe ser un numero con no mas de 2 decimales'})
    @IsPositive({message:'el precio es un numero positivo'})
    price:number;

    @IsNotEmpty({message:'el stock no puede estar vacio'})
    @IsNumber({},{message:'el stock debe ser un numero'})
    @Min(0,{message:'no puede ser negativo'})
    stock:number;

    //@IsNotEmpty({message:'imagen no debe estar vacio'})
    ////@IsArray({message:'images deb ser una lista'})
    //images:string[];

    @IsNotEmpty({message:'category no debe estar vacia'})
    category:string;
}
