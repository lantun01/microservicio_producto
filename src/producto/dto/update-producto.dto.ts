import { PartialType } from '@nestjs/mapped-types';
import { CreateProductoDto } from './create-producto.dto';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { UserEntity } from 'src/entities/user.entity';

export class UpdateProductoDto extends PartialType(CreateProductoDto) {
  //extiendo los atributos solo de create
  @IsNotEmpty()
  id: number;

  @IsOptional()
  testID: number;

}
