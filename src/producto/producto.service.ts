import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductoDto } from './dto/create-producto.dto';
import { UpdateProductoDto } from './dto/update-producto.dto';
import { Producto } from './entities/producto.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ProductoService {

  constructor(@InjectRepository(Producto)private readonly productRepository:Repository<Producto>
  ){}

  async create(createProdutDto: CreateProductoDto):Promise<Producto>{

    const product= this.productRepository.create(createProdutDto);
    return await this.productRepository.save(product);
  }

  async findAll():Promise<Producto[]> {
    return await this.productRepository.find();
  }

  async findOne(id: number) {
    const product= await this.productRepository.findOne({
      where: {id:id}
    });

    if(!product) throw new NotFoundException('producto no encontrado');
    return product;
  }


  async update( productoUpdate: UpdateProductoDto) {
    console.log('hola')
    //buscar producto
    const new_producto =await this.findOne(productoUpdate.id);
    //no encontrado
    if(!new_producto) throw new NotFoundException('producto no encontrada');
    //asignando los campos al parcial
    Object.assign(new_producto,productoUpdate);
    return await this.productRepository.save(new_producto);
  }
}


