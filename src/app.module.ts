import { Module } from '@nestjs/common';
import { ProductoModule } from './producto/producto.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { dataSourceOptions } from 'db/data-source';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [ProductoModule,TypeOrmModule.forRoot(dataSourceOptions),
    JwtModule.register({
      global: true,
      secret: process.env.ACCESS_TOKEN_SECRET_KEY,
      signOptions: { expiresIn: process.env.ACCESS_TOKEN_EXPIRE_TIME },
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
